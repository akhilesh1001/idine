//
//  ImageView.swift
//  IDine
//
//  Created by App on 13/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import UIKit


   func cornerImage(imageView : UIImageView){
        imageView.layoutIfNeeded()
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
   

}
