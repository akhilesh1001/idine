//
//  UIView+Addition.swift
//  IDine
//


import UIKit
import QuartzCore

// MARK: - Properties
public extension UIView {
    
    
    public func addShadowLayer(cornerRadius: CGFloat, shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, fillColor: UIColor = .white) {
        //Threads.performTaskAfterDealy(0.2) {
            if let arrayLayer = self.layer.sublayers {
                for layer in arrayLayer {
                    layer.removeFromSuperlayer()
                }
            }
            
            let shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor//UIColor.white.cgColor
            shadowLayer.shadowColor = shadowColor.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = offSet//CGSize(width: 2.6, height: 2.6)
            shadowLayer.shadowOpacity = opacity//0.8
            shadowLayer.shadowRadius = shadowRadius//8.0
            self.layer.insertSublayer(shadowLayer, at: 0)
        //}
    }
    
    public func initializeFromNib(nibNamed: String) {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNamed, bundle: bundle)
        if let view = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(view)
        }
    }
    
    public func initializeFromNibAndGetView(nibNamed: String)-> UIView?
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibNamed, bundle: bundle)
        if let view = nib.instantiate(withOwner:nil, options: nil).first as? UIView {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            return view
        }
        
        return nil
    }

    public func roundCorners(_ cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    
    public func addShadow(_ shadowRadius: CGFloat = 5.0, shadowOpacity: Float = 0.2) {
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
    }
    
    public func addShadow(ofColor color: UIColor = UIColor.black,radius: CGFloat = 3,
                          offset: CGSize = CGSize.zero,
                          opacity: Float = 0.5)
    {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = 1 //scale ? UIScreen.main.scale : 1
    }
    
    public func setViewShadow(color: UIColor,cornerRadius: CGFloat,shadowRadius: CGFloat = 5.0,shadowOpacity: Float = 0.2)
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowOpacity = shadowOpacity
    }
    
    
    public func makeLayer( color: UIColor, boarderWidth: CGFloat, round:CGFloat) -> Void {
        self.layer.borderWidth = boarderWidth;
        self.layer.cornerRadius = round;
        self.layer.masksToBounds =  true;
        self.layer.borderColor = color.cgColor
        
    }
    
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
     
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.layer.mask = shape
        
    }
    
    func animation(view:UIView)
    {
        view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5.0, options: .allowUserInteraction , animations: {
            view.transform = CGAffineTransform.identity
        }) { (fininsh) in
            
        }
    }
    
    func animateView()
    {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
        }) { (true) in
            UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.layoutIfNeeded()
            }) { (true) in
            }
        }
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.35, animations: {
            //            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }) { (finished : Bool) in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
    func applyGradient() -> Void {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor(red: 0, green: 95.0/255.0, blue: 181.0/255.0, alpha: 1.0).cgColor, UIColor(red: 23.0/255.0, green: 176.0/255.0, blue: 218.0/255.0, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.43)
        gradient.endPoint = CGPoint(x: 0.43, y: 1.0)
        gradient.cornerRadius = CGFloat(2.5)
        self.layer.insertSublayer(gradient, at: 0)
    }

    func roundTopCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func addTopRoundedCornerToView(targetView:UIView?, desiredCurve:CGFloat?)
    {
        let offset:CGFloat =  targetView!.frame.width/desiredCurve!
        let bounds: CGRect = targetView!.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y+bounds.size.height / 2, width: bounds.size.width, height: bounds.size.height / 2)
        
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y,width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        targetView!.layer.mask = maskLayer
    }
}
