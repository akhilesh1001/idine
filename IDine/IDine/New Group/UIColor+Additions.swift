//
//  UIColor+Additions.swift
//  IDine

import UIKit

extension UIColor {
    
    //229 36 43 106 180 220
    class var appNvaBarColor: UIColor{
        return  UIColor(red: 31.0/255.0, green: 24.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    }
    
    class var appBlueColor: UIColor{
        return  UIColor(red: 31.0/255.0, green: 24.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    }
    
    class var appLightGrayColor: UIColor{
        return  UIColor(red: 142.0/255.0, green: 142.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    }
    
    class var appGreenColor: UIColor{
        return  UIColor(red: 0, green: 163.0/255.0, blue: 75.0/255.0, alpha: 1.0)
    }
    
    class var appBorderColor: UIColor{
        return  UIColor(red: 137.0/255.0, green: 75.0/255.0, blue: 82.0/255.0, alpha: 1.0)
    }
    
    class var statusBarBlueColor: UIColor{
        return  UIColor(red: 43.0/255.0, green: 102.0/255.0, blue: 152.0/255.0, alpha: 1.0)
    }
    
    class var appLightBluleColor: UIColor{
        return  UIColor(red: 106.0/255.0, green: 180.0/255.0, blue: 220.0/255.0, alpha: 1.0)
    }
    
    class var appDarkBlueColor: UIColor{
        return  UIColor(red: 10.0/255.0, green: 72.0/255.0, blue: 125.0/255.0, alpha: 1.0)
    }
    
    class var docColor: UIColor {
        return UIColor(red: 106.0/255.0, green: 180.0/255.0, blue: 220.0/255.0, alpha: 1.0)
    }
    
    class var grayBorderColor: UIColor {
        return UIColor(red: 188.0/255.0, green: 188.0/255.0, blue: 188.0/255.0, alpha: 1.0)
    }
    
    class var appLightColor: UIColor{
        return  UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    
    class var presetedBgColor: UIColor {
        return UIColor.colorWithRGBA(redC: 20.0, greenC: 20.0, blueC: 20.0, alfa: 0.8)
    }
    
    class var homeStatusBgColor : UIColor {
       return UIColor(red: 181.0/255.0, green: 181.0/255.0, blue: 181.0/255.0, alpha: 1.0)
    }
    
    // MARK: UIColor additional properties
    class func colorWith(hexString:String)->UIColor {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.utf16.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
        
    class func colorWithRGBA(redC: CGFloat, greenC: CGFloat, blueC: CGFloat, alfa: CGFloat) -> UIColor {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = alfa
        red   = CGFloat(redC/255.0)
        green = CGFloat(greenC/255.0)
        blue  = CGFloat(blueC/255.0)
        alpha  = CGFloat(alpha)
        let color: UIColor =  UIColor(red:CGFloat(red), green: CGFloat(green), blue:CGFloat(blue), alpha: alpha)
        return color
    }
    
    class func colorWithAlpha(color: CGFloat, alfa: CGFloat) -> UIColor {
        let red:   CGFloat = CGFloat(color / 255.0)
        let green: CGFloat = CGFloat(color / 255.0)
        let blue:  CGFloat = CGFloat(color / 255.0)
        let alpha: CGFloat = alfa
        let color: UIColor =  UIColor(red:red, green:green, blue:blue, alpha:alpha)
        return color
    }
}
