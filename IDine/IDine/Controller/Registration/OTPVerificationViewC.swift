
//  OTPVerificationViewC.swift
//  Created by Akhilesh Gupta on 03/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import NKVPhonePicker
import SVProgressHUD

class OTPVerificationViewC: UIViewController , UITextFieldDelegate{
    
    //MARK:- Properties
    var txtField =  UITextField()
    let datePicker = UIDatePicker()
    var response = [User]()
    
    //MARK:- IBOutlet
    @IBOutlet weak var txtOcassionDate: UITextField!
    @IBOutlet weak var txtOcassionField: UITextField!
    @IBOutlet weak var txtDobField: UITextField!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var txtFieldFirstOTP: UITextField!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var txtFieldEditPhoneNumber: UITextField!
    @IBOutlet weak var enterOtpView: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var txtFieldSecondOTP: UITextField!
    @IBOutlet weak var txtFieldThirdOTP: UITextField!
    @IBOutlet weak var txtFieldFourthOTP: UITextField!
    @IBOutlet weak var txtFieldFifthOTP: UITextField!
    @IBOutlet weak var txtFieldSixtOTP: UITextField!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnCountryCodePicker: UIButton!
    @IBOutlet weak var txtFieldCountryCode: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        self.btnVerify.addShadow(6.0, shadowOpacity: 0.2)
        self.btnNext.addShadow(6.0, shadowOpacity: 0.2)
        self.lblPhoneNumber.text = "+91 "+response[0].mobileNumber!
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK:-Private Method
    private func ViewHidden()
    {
        self.btnVerify.isHidden = true
        self.enterOtpView.isHidden = true
        self.otpView.isHidden = true
        self.createdatePicker()
    }
    
    private func callTellUsMoreApi()
    {
        
        let userid = response[0].Id
        let dob = response[0].dob
        var ocationType = ""
        var ocationDate = ""
        if response[0].ocationType != ""
        {
            ocationType = response[0].ocationType!
        }
        else
        {
            ocationType = self.txtOcassionField.text!
        }
        if response[0].ocationDate != ""
        {
            ocationDate = response[0].ocationDate!
        }
        else
        {
            ocationDate = self.txtOcassionDate.text!
        }
        SVProgressHUD.show()
        let params = ["user_id": userid,
                      "dob": dob,
                      "occation_type": ocationType,
                      "occation_date" : ocationDate
            ]as Dictionary<String, AnyObject>
        SVProgressHUD.show()
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .signup(param: params))) { (response, success) in
            if success {
                print(response)
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                }
            }
        }
        
    }
    private func callLoginAPI() {
        let memberShipID = self.response[0].membershipId
        let phoneNumber = self.txtFieldEditPhoneNumber.text
         let params = ["m_number": memberShipID, "phone_number": Int(phoneNumber!)] as Dictionary<String, AnyObject>
        
        SVProgressHUD.show()
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .login(param: params))) { [weak self](response, success) in
            if success {
                SVProgressHUD.dismiss()
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                    if let token = user[0].token {
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.set(true, forKey: "MemberShipUser")
                        UserDefaults.standard.synchronize()
                        
                    }
                    //                    else {
                    //                        self?.alertUser(title: "Please email and password", message: "Please try again!")
                    //                    }
                }
                //                else{
                //                    self?.alertUser(title: "Plesse check network connection", message: "Please try again!")
                //                }
            }
            else
            {
                SVProgressHUD.dismiss()
                //print(response.)
            }
        }
    }
    
    
    private func setup(){
        txtFieldFirstOTP.delegate = self
        txtFieldSecondOTP.delegate = self
        txtFieldThirdOTP.delegate = self
        txtFieldFourthOTP.delegate = self
        txtFieldFifthOTP.delegate = self
        txtFieldSixtOTP.delegate = self
        txtOcassionDate.delegate = self
        txtDobField.delegate = self
        addActionToOtpTextField()
    }
    
    func addActionToOtpTextField()
    {
        txtFieldFirstOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged )
        txtFieldSecondOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged )
        txtFieldThirdOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        txtFieldFourthOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged )
        txtFieldFifthOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged )
        txtFieldSixtOTP.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txtFieldFirstOTP.becomeFirstResponder()
    }
    
    func createdatePicker()
    {
        datePicker.datePickerMode = .date
        txtDobField?.inputView = datePicker
        txtOcassionDate?.inputView = datePicker
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let donebutton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
        toolbar.setItems([donebutton], animated: true)
        txtDobField?.inputAccessoryView = toolbar
        txtOcassionDate?.inputAccessoryView = toolbar
    }
    @objc func doneClicked()
    {
        // format the date
        self.view.endEditing(true)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        //txtFieldDate.text = "\(datePicker.date)"
        if txtField == txtDobField
        {
            txtDobField?.text  = dateFormatter.string(from: datePicker.date)
            return
            
        }
        txtOcassionDate?.text  = dateFormatter.string(from: datePicker.date)
        
    }
    
    
    //MARK:- Public Method
    
    //MARK:- IBAction
    @IBAction func tapNextButton(_ sender: Any) {
        
        moveToHome()
    }
    
    @IBAction func tappedEditBtn(_ sender: Any) {
        if self.btnEdit.title(for: .normal) == "Edit"
        {
            self.btnEdit.setTitle("Ok", for: .normal)
            self.btnCountryCodePicker.isHidden = false
            self.lblPhoneNumber.isHidden = true
            self.txtFieldEditPhoneNumber.isHidden = false
            
        }
        else if self.btnEdit.title(for: .normal) == "Ok"
        {
         if let countryCode = btnCountryCodePicker.title(for: .normal)
            {
            self.lblPhoneNumber.text = "\(countryCode) "+"\(txtFieldEditPhoneNumber.text!)"
            }
         else{
            self.lblPhoneNumber.text = txtFieldEditPhoneNumber.text!
            }
            self.callLoginAPI()
            txtFieldEditPhoneNumber.text = ""
            self.btnCountryCodePicker.setTitle("", for: .normal)
            self.txtFieldEditPhoneNumber.isHidden = true
            self.btnCountryCodePicker.isHidden = true
            self.lblPhoneNumber.isHidden = false
            self.btnEdit.setTitle("Edit", for: .normal)
            self.txtFieldFirstOTP.becomeFirstResponder()
        }
        
    }
    
    @IBAction func tappedCountryCodeBtn(_ sender: Any) {
        self.txtFieldEditPhoneNumber.becomeFirstResponder()
        let storyBoard = UIStoryboard(name: "CountriesViewController", bundle: Bundle(for: CountriesViewController.self))
        let countryViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerVC") as! CountriesViewController
        countryViewController.delegate = self as CountriesViewControllerDelegate
        let navVC = UINavigationController(rootViewController: countryViewController)
        self.present(navVC, animated: true, completion: nil)
        
    }
    
    @IBAction func tapVerify(_ sender: Any) {
        
        let verifyOtp = "\(txtFieldFirstOTP.text!)"+"\(txtFieldSecondOTP.text!)"+"\(txtFieldThirdOTP.text!)"+"\(txtFieldFourthOTP.text!)"+"\(txtFieldFifthOTP.text!)"+"\(txtFieldSixtOTP.text!)"
        if self.response[0].otp == verifyOtp
        {
            if self.response[0].ocationDate == "" || self.response[0].ocationType == ""
            {
                self.ViewHidden()
                self.txtDobField.text = self.response[0].dob
                self.popupView.isHidden = false
            }
            else
            {
                moveToHome()
            }
        }
    }
    // Move to home
    private func moveToHome()
        
    {
        let sb = UIStoryboard(name: "Main" , bundle: nil)
        if let verifyViewC = sb.instantiateViewController(withIdentifier: "TabBarViewC") as? TabBarViewC
        {
            self.navigationController?.pushViewController(verifyViewC, animated: false)
        }
    }
    //MARK:- Table View Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtField = textField
    }
    
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if (text?.utf16.count)! == 1{
            switch textField{
            case txtFieldFirstOTP:
                txtFieldSecondOTP.becomeFirstResponder()
                break
            case txtFieldSecondOTP:
                txtFieldThirdOTP.becomeFirstResponder()
                break
            case txtFieldThirdOTP:
                txtFieldFourthOTP.becomeFirstResponder()
                break
            case txtFieldFourthOTP:
                txtFieldFifthOTP.becomeFirstResponder()
                break
            case txtFieldFifthOTP:
                txtFieldSixtOTP.becomeFirstResponder()
                break
            case txtFieldSixtOTP:
                txtFieldSixtOTP.resignFirstResponder()
                break
            default:
                break
            }
        }else{
            
        }
    }
}

extension OTPVerificationViewC: CountriesViewControllerDelegate {
    func countriesViewControllerDidCancel(_ sender: CountriesViewController){
        sender.dismiss(animated: true, completion: nil)
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = country.flag
        self.btnCountryCodePicker.setTitle("+"+country.phoneExtension, for: .normal)
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country, flagImage:UIImage){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = flagImage
        self.btnCountryCodePicker.setTitle("+"+country.phoneExtension, for: .normal)
    }
}
