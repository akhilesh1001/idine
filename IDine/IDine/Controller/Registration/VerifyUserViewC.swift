//
//  VerifyUserViewC.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import NKVPhonePicker
import SVProgressHUD
import SwiftHTTP
import  Alamofire
import SwiftyUserDefaults


class VerifyUserViewC: UIViewController {
    
    //MARK:- Properties
    var response = [[String : AnyObject]]()
    
    //MARK:- IBOutlet
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var txtFieldMembershipNumber: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var txtFieldCountryCode: UITextField!
    
    //Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.btnVerify.addShadow(6.0, shadowOpacity: 0.2)
        
        
    }
    
    //MARK:- Private Function
    private func callLoginAPI() {
        
        let memberShipID = txtFieldMembershipNumber.text, phoneNumber = txtFieldPhoneNumber.text
        if memberShipID?.isEmpty == true {
            self.alertUser(title: "Please enter MemberShip id", message: "Please try again!")
            return
        }
        if phoneNumber?.isEmpty == true {
            self.alertUser(title: "Please enter Phone number", message: "Please try again!")
            return
        }
        let params = ["m_number": memberShipID, "phone_number": phoneNumber] as Dictionary<String, AnyObject>
        
        SVProgressHUD.show()
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .login(param: params))) { [weak self](response, success) in
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                    if let token = user[0].token {
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.set(true, forKey: "MemberShipUser")
                        UserDefaults.standard.synchronize()
                        SVProgressHUD.dismiss()
                        self?.moveToOTP(user: user[0])
                    }
                    //                    else {
                    //                        self?.alertUser(title: "Please email and password", message: "Please try again!")
                    //                    }
                }
                //                else{
                //                    self?.alertUser(title: "Plesse check network connection", message: "Please try again!")
                //                }
            }
        }
    }
    
    
    private func moveToOTP(user: User?){
        let mainSb = UIStoryboard(name: "Main", bundle: nil)
        if let otpVC = mainSb.instantiateViewController(withIdentifier: "OTPVerificationViewC") as? OTPVerificationViewC {
            otpVC.response = [user!]
            self.navigationController?.pushViewController(otpVC, animated: true)
            
        }
    }
    //Function to alert the User of the Error
    
    private func alertUser(title: String, message: String){
        
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        
        let OK = UIAlertAction(title:"OK", style: .default, handler: nil)
        
        alert.addAction(OK)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- Public Function
    
    //MARK:- IBAction
    @IBAction func tappedCountryCode(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "CountriesViewController", bundle: Bundle(for: CountriesViewController.self))
        let countryViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerVC") as! CountriesViewController
        countryViewController.delegate = self as? CountriesViewControllerDelegate
        let navVC = UINavigationController(rootViewController: countryViewController)
        self.present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSendOTPBtn(_ sender: Any) {
        // handleLoginAPI()
        callLoginAPI()
    }
}

//MARK:- Extension
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension VerifyUserViewC: CountriesViewControllerDelegate {
    func countriesViewControllerDidCancel(_ sender: CountriesViewController){
        sender.dismiss(animated: true, completion: nil)
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = country.flag
        self.txtFieldCountryCode.text = "+"+country.phoneExtension
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country, flagImage:UIImage){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = flagImage
        self.txtFieldCountryCode.text = "+"+country.phoneExtension
    }
}

