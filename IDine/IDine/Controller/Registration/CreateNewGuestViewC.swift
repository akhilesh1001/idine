//
//  CreateNewGuestViewC.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import NKVPhonePicker
import SVProgressHUD
class CreateNewGuestViewC: UIViewController {
    
    //MARK:- Properties
    
    //MARK:- IBOutlet
    @IBOutlet weak var txtFieldCountryCode: UITextField!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhoneNumber: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.btnSubmit.addShadow(6.0, shadowOpacity: 0.2)
        
    }
    //MARK:- Private Method
    
    
    private func moveToHomeViewC(){
        let mainSb = UIStoryboard(name: "Main", bundle: nil)
        if let homeViewC = mainSb.instantiateViewController(withIdentifier: "TabBarViewC") as? TabBarViewC {
            self.navigationController?.pushViewController(homeViewC, animated: true)
            
        }
    }
    //Function to alert the User of the Error
    
    private func alertUser(title: String, message: String){
        
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        
        let OK = UIAlertAction(title:"OK", style: .default, handler: nil)
        
        alert.addAction(OK)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:-Public Method
    
    //MARK:- IBAction
    
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedCountryCode(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "CountriesViewController", bundle: Bundle(for: CountriesViewController.self))
        let countryViewController = storyBoard.instantiateViewController(withIdentifier: "CountryPickerVC") as! CountriesViewController
        countryViewController.delegate = self as CountriesViewControllerDelegate
        let navVC = UINavigationController(rootViewController: countryViewController)
        self.present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func tappedSubmitBtn(_ sender: Any) {
        //moveToHomeViewC()
        signup()
    }
    
    @IBAction func tapInfoBtn(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let infoViewC = sb.instantiateViewController(withIdentifier: "MoreInfoTitle") as? MoreInfoTitle
        {
            self.navigationController?.pushViewController(infoViewC, animated: true)
        }
    }
    
    private func signup() {
        let name = self.txtFieldName.text, email = self.txtFieldEmail.text,phoneNumber = self.txtFieldPhoneNumber.text
        
        if name?.isEmpty == true {
            self.alertUser(title: "Please enter first name", message: "Please try again!")
            return
        }
        if phoneNumber?.isEmpty == true {
            self.alertUser(title: "Please enter phone number", message: "Please try again!")
            return
        }
        SVProgressHUD.show()
        let params = ["name": name,
                      "email": email,
                      "phone": phoneNumber
            ]as Dictionary<String, AnyObject>
        
        SVProgressHUD.show()
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .signup(param: params))) { (response, success) in
            if success {
                print(response)
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                    if let token = user[0].token {
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.set(true, forKey: "MemberShipUser")
                        UserDefaults.standard.synchronize()
                        self.moveToHomeViewC()
                    }
                    
                }
            }
        }
    }
    
}

extension CreateNewGuestViewC: CountriesViewControllerDelegate {
    func countriesViewControllerDidCancel(_ sender: CountriesViewController){
        sender.dismiss(animated: true, completion: nil)
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = country.flag
        self.txtFieldCountryCode.text = "+"+country.phoneExtension
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country, flagImage:UIImage){
        sender.dismiss(animated: true, completion: nil)
        //self.imgViewFlag.image = flagImage
        self.txtFieldCountryCode.text = "+"+country.phoneExtension
    }
}
