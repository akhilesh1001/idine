//
//  ProfileViewC.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit

class ProfileViewC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblValidDateTill: UILabel!
    @IBOutlet weak var lblMemberShipNo: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    
    //MARK:- Properties
    var dataArr = [[String : AnyObject]]()
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        loadData()
    }
    
    //MARK:- Private Method
    private  func registerNib()
    {
        let tblnib = UINib(nibName: "ProfileCell", bundle: nil)
        self.tblView.register(tblnib, forCellReuseIdentifier: "ProfileCell")
        
    }
    private func loadData(){
        let tblDict = ["Heading": "My Bookings" as AnyObject, "image": #imageLiteral(resourceName: "mybooking")  as AnyObject]
        let tblDict2 = ["Heading": "E-Vouchers" as AnyObject,  "image": #imageLiteral(resourceName: "Voucher") as AnyObject]
        let tblDict3 = ["Heading": "Notifications" as AnyObject, "image": #imageLiteral(resourceName: "Notification") as AnyObject]
        let tblDict4 = ["Heading": "Usage history" as AnyObject, "image": #imageLiteral(resourceName: "history") as AnyObject]
        let tblDict5 = ["Heading": "Customer Support" as AnyObject,  "image": #imageLiteral(resourceName: "support") as AnyObject]
        let tblDict6 = ["Heading": "FAQ" as AnyObject, "image": #imageLiteral(resourceName: "faq") as AnyObject]
        let tblDic7 = ["Heading": "Tems & Conditions" as AnyObject, "image": #imageLiteral(resourceName: "terms&condition") as AnyObject]
        let tblDic8 = ["Heading": "Logout" as AnyObject, "image": #imageLiteral(resourceName: "terms&condition") as AnyObject]
        dataArr.append(contentsOf: [tblDict, tblDict2, tblDict3, tblDict4, tblDict5, tblDict6, tblDic7])
        self.tblView.reloadData()
        
    }
    
    
    private func movetoUsageHistory()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewC = sb.instantiateViewController(withIdentifier: "UsageHistroyViewC") as? UsageHistroyViewC
        {
            self.navigationController?.pushViewController(viewC, animated: true)
        }
    }
    private func movetoMyBooking()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewC = sb.instantiateViewController(withIdentifier: "MyBookingViewC") as? MyBookingViewC
        {
            self.navigationController?.pushViewController(viewC, animated: true)
        }
        
    }
    private func moveToEvoucher()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewC = sb.instantiateViewController(withIdentifier: "EVoucherViewC") as? EVoucherViewC
        {
            self.navigationController?.pushViewController(viewC, animated: true)
        }
    }
    
    
}

extension ProfileViewC: UITableViewDataSource,UITableViewDelegate,NextButtonClickedDeligate
{
    //MARK:- Table View Delegate Method
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tblView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as? ProfileCell
        {
            cell.nextButtonClickedDeligate = self
            cell.selectionStyle = .none
            let dict = dataArr[indexPath.row]
            if indexPath.row == 0
            {
                cell.lblNumberOfBooking.text = "05"
            }
            else
            {
                cell.lblNumberOfBooking.text = " "
            }
            
            if let Heading = dict["Heading"] as? String
            {
                cell.lblHeading.text = Heading
            }
            
            if let image1 = dict["image"] as? UIImage
            {
                cell.imgView.image = image1
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row
        {
        case 0:
            movetoMyBooking()
        case 1:
            moveToEvoucher()
        case 3:
            movetoUsageHistory()
        default:
            return
        }
        
        
    }
    //MARK:- Protocal Method
    func nextButtonClicked(cell: UITableViewCell) {
        
    }
    
}



