//
//  ViewController.swift
//  IDine
//
//  Created by App on 10/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import SwiftyUserDefaults


//MARK: - Testing Purpose
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        // callLoginAPI()
        //callHomeBanner()
        getResturant()
        //getdetail()
        //gethomeBanner()
        //getOffer()
    }
    
    private func callLoginAPI() {
        
        var params: APIParams = [:]
        params["m_number"] = 7385255 as AnyObject
        params["phone_number"] = 987654345 as AnyObject
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .login(param: params))) { [weak self](response, success) in
            guard let strongSelf = self else {
                return
            }
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                    if let token = user[0].token {
                        //Defaults[.token] = token
                        UserDefaults.standard.set(token, forKey: "token")
                        UserDefaults.standard.synchronize()
                        strongSelf.callHomeBanner()
                    }
                }
                
            }
        }
    }
    
    private func callHomeBanner() {
        
        var params: APIParams = [:]
        params["m_number"] = 7385255 as AnyObject
        params["phone_number"] = 987654345 as AnyObject
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .resendOtp(param: params))) { (response, success) in
            if success {
                print(response)
                if let safeResponse = response as? [[String : AnyObject]], let user = User.parseData(data: [safeResponse[0]]) {
                    //                        if let token = user.token {
                    //                            //Defaults[.token] = token
                    //                            UserDefaults.standard.set(token, forKey: "token")
                    //                        }
                }
                
            }
        }
    }
    private func gethomeBanner()
    {
        
        // SVProgressHUD.show()
        
        
        let params = ["user_type": "guest"] as Dictionary<String, AnyObject>
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .getHomeBanner())) { [weak self](response, success) in
            if success {
                //if let safeResponse = response as? [[String : AnyObject]], let home = HomeBanner.parseData(data: [[safeResponse]])
                if let safeResponse = response as? [[String : AnyObject]]{
                    print(safeResponse)
                    //                    else {
                    //                        self?.alertUser(title: "Please email and password", message: "Please try again!")
                    //                    }
                }
                //                else{
                //                    self?.alertUser(title: "Plesse check network connection", message: "Please try again!")
                //                }
            }
        }
    }
    
    private func getOffer()
    {
        // SVProgressHUD.show()
        
        
        let params = ["user_type": "guest"] as Dictionary<String, AnyObject>
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .offer())) { [weak self](response, success) in
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let offer = Offer.parseData(data: safeResponse)
                {
                    //                    self?.offer = offer
                    //                    self?.tableView.reloadData()
                    //                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    private func getResturant()
    {
        //            var params: APIParams = [:]
        //         params["m_number"] = 7385255 as AnyObject
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .getResturant())) { (response, success) in
            if success {
                print(response)
                
            }
        }
    }
    
    func getdetail(){
        //let url = kGetGroupDetailsById + "?groupid=" + String(self.groupId)
        NetworkEngine.sharedInstance.getRequestHandler(kHomeBannerUrl, isBaseAuthorized: false) { ( response: AnyObject?, error:NSError?, httpStatusCode:Int?) in
            DispatchQueue.main.async(execute: {
                //SVProgressHUD.dismiss()
                if error == nil && response != nil && response is NSDictionary {
                    let dictResponse = response as! NSDictionary
                    if let success = dictResponse["success"] as? Bool , success == true {
                        print(dictResponse)
                    }
                    else {
                        //self.alertUser(title: "Error...", message: "Please try again!")
                    }
                }else {
                    //self.alertUser(title: "Error...", message: "Please try again!")
                }
            })
        }
    }
}
