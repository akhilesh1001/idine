//
//  HomeViewC.swift
//  IDine
//  Created by App on 04/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class HomeViewC: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnNotification: UIButton!
    
    //MARK: - Propeties
    var tblData = [[String : AnyObject]]()
    var dataArr = [[String : AnyObject]]()
    var homebanner = [HomeBanner]()
    var offer = [Offer]()
    var isGuest : Bool?
    
    //MARK : - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        self.hideKeyboardWhenTappedAround()
        gethomeBanner()
        getOffer()
    }
    
    //MARK: - Private Method
    private  func registerNib()
    {
        let tblnib = UINib(nibName: "ResturantCell", bundle: nil)
        self.tableView.register(tblnib, forCellReuseIdentifier: "ResturantCell")
        let colNib = UINib(nibName: "BookingCell", bundle: nil)
        self.collectionView.register(colNib, forCellWithReuseIdentifier: "BookingCell")
        loadData()
        
    }
    private func loadData()
    {
        let firstDict = ["title": "LOREIM TITLE SCG " as AnyObject, "subtitle": "LOREIM TITLE SCG" as AnyObject, "image": #imageLiteral(resourceName: "images-4") as AnyObject]
        let Dict2 = ["title": "Radinous Blue" as AnyObject, "subtitle": "Five star resturant" as AnyObject, "image": #imageLiteral(resourceName: "images-2") as AnyObject]
        let Dict3 = ["title": "Country In" as AnyObject, "subtitle": "Five star resturant" as AnyObject, "image": #imageLiteral(resourceName: "restra_two") as AnyObject]
        dataArr.append(contentsOf: [firstDict, Dict2, Dict3])
        let tblDict = ["resturant": "Paloma" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "restra_two") as AnyObject]
        let tblDict2 = ["resturant": "Lava" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "restra_one") as AnyObject]
        let tblDict3 = ["resturant": "Tea Lounge" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "images-2") as AnyObject]
        tblData.append(contentsOf: [tblDict, tblDict2, tblDict3])
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    
    private func moveToOffer()
    {
        let offerSb = UIStoryboard(name: "Main", bundle: nil)
        if let offerView = offerSb.instantiateViewController(withIdentifier: "OfferViewC") as? OfferViewC
        {
            self.navigationController?.pushViewController(offerView, animated: true)
        }
    }
    private  func moveToSignup()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let signupViewC = sb.instantiateViewController(withIdentifier: "CreateNewGuestViewC") as? CreateNewGuestViewC
        {
            self.navigationController?.pushViewController(signupViewC, animated: true)
        }
    }
    private  func moveToBooking()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let bookingViewc = sb.instantiateViewController(withIdentifier: "TableBookDetailsViewC") as? TableBookDetailsViewC
        {
            self.navigationController?.pushViewController(bookingViewc, animated: true)
        }
    }
    //Function to alert the User of the Error
    
    private func alertUser(title: String, message: String){
        let alert = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let OK = UIAlertAction(title:"OK", style: .default, handler: nil)
        alert.addAction(OK)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK:-IBAction
    @IBAction func tapSeeAll(_ sender: Any) {
        moveToOffer()
    }
    
    @IBAction func tapFavourate(_ sender: Any) {
        
    }
    
    //Get home banner data
    
    func gethomeBanner()
    { SVProgressHUD.show()
        let params = ["user_type": "guest"] as Dictionary<String, AnyObject>
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .getHomeBanner())) { [weak self](response, success) in
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let home = HomeBanner.parseData(data: safeResponse)
                {
                    self?.homebanner = home
                    self?.collectionView.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    // Get offer data from api
    func getOffer()
    {
        SVProgressHUD.show()
        let params = ["user_type": "guest"] as Dictionary<String, AnyObject>
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .offer())) { [weak self](response, success) in
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let offer = Offer.parseData(data: safeResponse)
                {
                    self?.offer = offer
                    self?.tableView.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
}


extension HomeViewC:UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,NextPageDeligate,BookNowDeligate
{
    //MARK:Protocal Method
    func btnNextClicked(cell: UITableViewCell) {
        
    }
    
    func btnBookNowClicked(cell: UICollectionViewCell) {
        if UserDefaults.standard.bool(forKey: "MemberShipUser") == true
        {
            moveToBooking()
        }
        else
        {
            moveToSignup()
        }
        
    }
    
    //MARK:- Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "ResturantCell", for: indexPath) as? ResturantCell
        {
            cell.selectionStyle = .none
            cell.nextPageDeligate = self
            let dict = offer[indexPath.row]
            if let name = dict.rname
            {
                cell.lblResturantName.text = name
            }
            if let offer = dict.offerName
            {
                cell.lblOffer.text = offer
            }
            if let offerdetail = dict.offerDescription
            {
                cell.lblRestaurantOffer.text = offerdetail
            }
            if let image = dict.offerImage
            {
                let imgUrl = URL(string: "\(imageBaseUrl)"+"\(image)")
                cell.imgView.af_setImage(withURL: imgUrl!)
                
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 146.0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homebanner.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell" , for: indexPath) as? BookingCell
        {
            cell.bookNowDeligate = self
            let dict = homebanner[indexPath.row]
            if let rname = dict.restaurantName
            {
                cell.lblTitle.text = rname
            }
            if let subtitle = dict.restaurantSubtitle
            {
                cell.lblSubTitle.text = subtitle.uppercased()
            }
            if let image = dict.restaurantImage
            {
                let imgUrl = URL(string: "\(imageBaseUrl)"+"\(image)")
                cell.imgView.af_setImage(withURL: imgUrl!)
                
            }
            
            if dict.bannerStatus == 0
            {
                cell.btnBookNow.isHidden = true
            }
            
            return cell
            
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 340, height: 280)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewC = sb.instantiateViewController(withIdentifier: "BookingTableViewC") as? BookingTableViewC
        {
            if let ResturantRefId = homebanner[indexPath.row].restaurantRefId
            {
                viewC.resturantId = Int(ResturantRefId)
                self.navigationController?.pushViewController(viewC, animated: true)
            }
            
        }
    }
}
