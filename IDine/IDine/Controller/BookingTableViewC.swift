
//  BookingTableViewC.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//
import UIKit
import SVProgressHUD

class BookingTableViewC: UIViewController {
    
    //MARK: - Properties
    var resturantId : Int?
    var restaurantDetail = [RestaurantDetail]()
    
    
    //MARK: - IBoutlet
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        registerNib()
        getRestaurantDetail()
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK: - Private Method
    private func configureTableView() {
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.estimatedRowHeight = 120.0
        
        
    }
    private func registerNib()
    {
        let tblNib = UINib(nibName: "BookingTableCell", bundle: nil)
        self.tblView.register(tblNib, forCellReuseIdentifier: "BookingTableCell")
        
    }
    
    private func moveToBooking()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let bookingViewc = sb.instantiateViewController(withIdentifier: "TableBookDetailsViewC") as? TableBookDetailsViewC
        {
            self.navigationController?.pushViewController(bookingViewc, animated: true)
        }
    }
    
    private  func moveToSignup()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let signupViewC = sb.instantiateViewController(withIdentifier: "CreateNewGuestViewC") as? CreateNewGuestViewC
        {
            self.navigationController?.pushViewController(signupViewC, animated: true)
        }
    }
    
    //Func get Restaurant Detail
    private  func getRestaurantDetail()
    {
        SVProgressHUD.show()
        let params = ["rest_id": resturantId] as Dictionary<String, AnyObject>
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .getResturantDetail(param: params))) { [weak self](response, success) in
            if success {
                if let safeResponse = response as? [[String : AnyObject]], let resturantDetail = RestaurantDetail.parseData(data: safeResponse)
                {
                    self!.restaurantDetail = resturantDetail
                    print(resturantDetail.count)
                    SVProgressHUD.dismiss()
                    self?.tblView.reloadData()
                }
            }
        }
    }
    
    //MARK: - Public Method
    
    //MARK: - IBAction
    
}


extension BookingTableViewC:UITableViewDelegate,UITableViewDataSource,BookingTableButtonDelegate
{
    //MARK: - Protocal Method
    func tappedBookNowBtnClicked(cell: UITableViewCell) {
        
        if UserDefaults.standard.bool(forKey: "MemberShipUser") == true
        {
            moveToBooking()
        }
        else
        {
            moveToSignup()
        }
    }
    
    func tappedbakBtnClicked(cell: UITableViewCell) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tappedDownloadBtnClicked(cell: UITableViewCell) {
        
    }
    
    //MARK: - Table  View Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  let cell = self.tblView.dequeueReusableCell(withIdentifier: "BookingTableCell", for: indexPath) as? BookingTableCell
        {
            cell.selectionStyle = .none
            
            let dict = restaurantDetail[indexPath.row]
            
            cell.restaurantDishType = dict.disheType
            cell.restaurantOffer = dict.offer
            cell.collectionView.reloadData()
            cell.dishTypeCollectionView.reloadData()
            cell.bookingTableButtonDelegate = self
            cell.ratingView.rating = 4.0
            cell.lblRatingDescription.text = "\(4.0) star Restaurant"
            
            if let image = dict.image
            {
                if let imgUrl = URL(string: "\(imageBaseUrl)"+"\(image)") {
                    cell.imgView.af_setImage(withURL: imgUrl)
                }
                
            }
            if let restaurantName = dict.name
            {
                cell.lblRestaurantName.text = restaurantName
            }
            if let title = dict.subtitle
            {
                cell.lblRestaurantSubtitle.text = title.uppercased()
            }
            if let opentime = dict.openTime
            {
                cell.lblRestaurantOpeningTime.text = opentime
            }
            if let address = dict.address
            {
                cell.lblRestaurantAddress.text = address
            }
            if let contactNumber = dict.contactNumber
            {
                cell.lblRestaurantContactNo.text = contactNumber
            }
            return cell
        }
        
        
        return UITableViewCell()
    }
    
    
}
