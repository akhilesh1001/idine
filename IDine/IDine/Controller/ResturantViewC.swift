//
//  ResturantViewC.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class ResturantViewC: UIViewController {
    
    
    //MARK:-IBOutlet
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:-Properties
    var dataArr = [[String : AnyObject]]()
    var myArray = [[String : AnyObject]]()
    var restaurant = [Restaurant]()
    
    
    //MARK:-Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self
        registerNib()
        myArray = dataArr
        self.hideKeyboardWhenTappedAround()
        getResturant()
    }
    
    //MARK:- Private Method
    private   func registerNib()
    {
        let tblnib = UINib(nibName: "ResturantSearchCell", bundle: nil)
        self.tblView.register(tblnib, forCellReuseIdentifier: "ResturantSearchCell")
        loadData()
    }
    private func loadData()
    {
        let tblDict = ["resturant": "Coral" as AnyObject, "address": "IHG DOHA" as AnyObject, "image": #imageLiteral(resourceName: "restra_two") as AnyObject]
        let tblDict2 = ["resturant": "Mykonos" as AnyObject, "address": "IHG DOHA" as AnyObject, "image": #imageLiteral(resourceName: "restra_one") as AnyObject]
        let tblDict3 = ["resturant": "Tea Lounge" as AnyObject, "address": "New Delhi" as AnyObject, "image": #imageLiteral(resourceName: "images-4") as AnyObject]
        dataArr.append(contentsOf: [tblDict, tblDict2, tblDict3])
        self.tblView.reloadData()
        
    }
    
    //MARK:-Public Method
    
    //MARK:- IBAction
    
    //Mark:- Api call
    func getResturant()
    {
        SVProgressHUD.show()
        
        
        let params = ["user_type": "guest"] as Dictionary<String, AnyObject>
        
        APIManager.shared.request(apiRouter: APIRouter.init(endpoint: .getResturant())) { [weak self](response, success) in
            if success {
                
                if let safeResponse = response as? [[String : AnyObject]], let resturant = Restaurant.parseData(data: safeResponse)
                {
                    self?.restaurant = resturant
                    self?.tblView.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
}


extension ResturantViewC:  UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,ResturantSearchDeligate
{
    //MARK:- Protocal Method
    
    func btnItalianClicked(cell: UITableViewCell) {
        
    }
    
    func btnChineseClicked(cell: UITableViewCell) {
        
    }
    
    //MARK:- Table View  Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = self.tblView.dequeueReusableCell(withIdentifier: "ResturantSearchCell", for: indexPath) as? ResturantSearchCell
        {
            cell.selectionStyle = .none
            cell.resturantSearchDeligate = self
            let dict = restaurant[indexPath.row]
            if let name = dict.name
            {
                cell.lblResturantName.text = name
            }
            if let subtitle = dict.subtitle
            {
                cell.lblResturantSubTitle.text = subtitle.uppercased()
            }
            if let image = dict.image
            {
                if let imgUrl = URL(string: "\(imageBaseUrl)"+"\(image)")
                {
                    cell.imgView.af_setImage(withURL: imgUrl)
                }
                
            }
            if dict.disheType.count > 0
            {
                cell.dishdata = dict.disheType
                cell.collectionView.reloadData()
            }
            return cell
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let viewC = sb.instantiateViewController(withIdentifier: "BookingTableViewC") as? BookingTableViewC
        {
            viewC.resturantId = restaurant[indexPath.row].Id
            self.navigationController?.pushViewController(viewC, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 335
    }
    
    
    //MARK:- Search Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        myArray.removeAll()
        tblView.reloadData()
        let searchStr = searchText.lowercased()
        
        if searchStr.isEmpty {
            myArray.append(contentsOf: dataArr)
            tblView.reloadData()
            return
        }
        
        let resturant = dataArr.filter { (myDict) -> Bool in
            if let resturant = myDict["resturant"]?.lowercased, let address = myDict["address"]?.lowercased {
                if resturant.contains(searchStr) || address.contains(searchStr)  {
                    
                    return true
                }
            }
            return false
        }
        myArray.append(contentsOf: resturant)
        tblView.reloadData()
        
    }
}
