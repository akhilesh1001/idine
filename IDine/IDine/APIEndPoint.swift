//
//  APIEndPoint.swift
//  AV
//


import Foundation
import Alamofire

enum APIEndpoint {
    case none
    case login(param: APIParams)
    case signup(param: APIParams)
    case resendOtp(param: APIParams)
    case membershipRequest()
    case editPhone(param: APIParams)
    case offer()
    case getResturant()
    case getHomeBanner()
    case getResturantDetail(param: APIParams)
    case tellMeAboutYourself(param: APIParams)
}

