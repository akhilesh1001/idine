//
//  User.swift
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, Mappable  {
    // MARK: - Properties
    
    var Id: Int?
    var name: String?
    var dob: String?
    var membershipId : String?
    var mobileNumber: String?
    var email: String?
    var ocationDate : String?
    var ocationType: String?
    var otp: String?
    var token: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Id <- map["user_id"]
        name <- map["name"]
        dob <- map["dob"]
        membershipId <- map["m_number"]
        mobileNumber <- map["phone_number"]
        email <- map["email"]
        ocationDate <- map["occation_date"]
        ocationType <- map["occation_type"]
        otp <- map["otp"]
        token <- map["x-access-token"]
    }

}

extension User {
    
//    class func parseData(data: [String: AnyObject]) -> User? {
//        return Mapper<User>().map(JSON: data)
//    }
    class func parseData(data: [[String: AnyObject]]) -> [User]? {
        return Mapper<User>().mapArray(JSONArray:data)
    }
}
