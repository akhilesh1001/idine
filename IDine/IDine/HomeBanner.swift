//
//  HomeBanner.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import  ObjectMapper

class HomeBanner: NSObject, Mappable  {
    
    // MARK: - Properties
    var Id: Int?
    var restaurantName: String?
    var restaurantSubtitle: String?
    var restaurantId : String?
    var modeOfRestaurant: String?
    var restaurantImage: String?
    var bannerStatus : Int?
    var restaurantRefId: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Id <- map["id"]
        restaurantName <- map["name"]
        restaurantSubtitle <- map["c_name"]
        restaurantId <- map["ref_id"]
        modeOfRestaurant <- map["mode"]
        restaurantImage <- map["banner_img"]
        bannerStatus <- map["banner_status"]
        restaurantRefId <- map["ref_id"]
    }
    
}

extension HomeBanner {
    
    
    class func parseData(data: [[String: AnyObject]]) -> [HomeBanner]? {
        return Mapper<HomeBanner>().mapArray(JSONArray:data)
    }
}
