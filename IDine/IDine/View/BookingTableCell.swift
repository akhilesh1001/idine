import UIKit
import Cosmos
import AlamofireImage

protocol BookingTableButtonDelegate : class {
    func tappedbakBtnClicked(cell : UITableViewCell)
    func tappedDownloadBtnClicked(cell : UITableViewCell)
    func tappedBookNowBtnClicked(cell : UITableViewCell)
    
}

class BookingTableCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblRestaurantAddress: UILabel!
    @IBOutlet weak var lblRestaurantContactNo: UILabel!
    @IBOutlet weak var dishTypeCollectionView: UICollectionView!
    @IBOutlet weak var lblRestaurantOpeningTime: UILabel!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblRestaurantSubtitle: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var lblRatingDescription: UILabel!
    var restaurantOffer = [RestaurantDetail.OfferData]()
    var restaurantDishType = [String]()
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    weak var bookingTableButtonDelegate : BookingTableButtonDelegate?
    var dataArr = [[String : AnyObject]]()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        registerNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.dishTypeCollectionView.delegate = self
        self.dishTypeCollectionView.dataSource = self
       
    }
    
    
    
    func registerNib()
    {
        
        
//        let tblNib = UINib(nibName: "OfferViewCell", bundle: nil)
//        self.collectionView.register(tblNib, forCellWithReuseIdentifier: "OfferViewCell")
//
        let nib = UINib(nibName: "DishTypeCollectionCell", bundle: nil)
        self.dishTypeCollectionView.register(nib, forCellWithReuseIdentifier: "DishTypeCollectionCell")
        
        
        let tblColNib = UINib(nibName: "OfferCell", bundle: nil)
       self.collectionView.register(tblColNib, forCellWithReuseIdentifier: "OfferCell")
        
        loadData()
    }
    
    
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        if let mydelegate = self.bookingTableButtonDelegate
        {
            mydelegate.tappedbakBtnClicked(cell: self)
        }
    }
    
    
    @IBAction func tappedBookNowBtn(_ sender: Any) {
        if let mydelegate = self.bookingTableButtonDelegate
        {
            mydelegate.tappedBookNowBtnClicked(cell: self)
        }
    }
    @IBAction func tappedDownloadBtn(_ sender: Any) {
        if let mydelegate = self.bookingTableButtonDelegate
        {
            mydelegate.tappedDownloadBtnClicked(cell: self)
        }
    }
    func loadData()
    {
        let tblDict = ["resturant": "Coral" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "images-2") as AnyObject]
        let tblDict2 = ["resturant": "Mykonos" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "images-3") as AnyObject]
        let tblDict3 = ["resturant": "Tea Lounge" as AnyObject, "offer": "20% off on Fodd Bill Valid only on Inhouse dining" as AnyObject, "image": #imageLiteral(resourceName: "images-4") as AnyObject]
        dataArr.append(contentsOf: [tblDict, tblDict2, tblDict3])
        self.collectionView.reloadData()
        
        
    }
    
    
}
extension BookingTableCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView
        {
          return restaurantOffer.count
        }
        else if collectionView == self.dishTypeCollectionView
        {
           return restaurantDishType.count
        }
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView
        {
            if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "OfferCell", for: indexPath) as? OfferCell
            {
                let dict = restaurantOffer[indexPath.row]
                if let title = dict.restaurantName
                {
                    cell.lblResturant.text = title
                }
                if let offername = dict.offername
                {
                    cell.lblOffer.text = offername
                }
                if let offerDesc = dict.description
                {
                    cell.lblOfferDescription.text = offerDesc
                }
                if let image = dict.image 
                {
                    let imgUrl = URL(string: "\(imageBaseUrl)"+"\(image)")
                    
                    cell.imgView.af_setImage(withURL: imgUrl!)
                }
                return cell
            }
        }
        
        else if collectionView == self.dishTypeCollectionView
        {
            if let cell = self.dishTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "DishTypeCollectionCell", for: indexPath) as? DishTypeCollectionCell
            {
                cell.lblDishType.text = restaurantDishType[indexPath.row]
                return cell
            }
        }
         return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView
        {
           return CGSize(width: collectionView.frame.size.width, height: 152)
        }
        else if collectionView == self.dishTypeCollectionView
        {
            return CGSize(width: 130, height: 50)
        }
        return CGSize()
    }
}
