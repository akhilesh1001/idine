//
//  OfferCell.swift
//  IDine
//
//  Created by App on 04/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit

class OfferCell: UICollectionViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var lblOfferDescription: UILabel!
    @IBOutlet weak var lblResturant: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblOffer: UILabel!
    @IBOutlet weak var lblValidOnly: UILabel!
    
    //MARK:- Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

}
