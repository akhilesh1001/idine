//
//  DishType.swift
//  IDine
//
//  Created by App on 12/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit

class DishTypeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblDishType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDishType.makeLayer(color: UIColor.appBorderColor, boarderWidth:1.0, round: 6.0)
        // Initialization code
    }

}
