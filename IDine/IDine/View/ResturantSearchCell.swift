//
//  ResturantSearchCell.swift
//  IDine
//
//  Created by App on 04/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import UIKit
protocol ResturantSearchDeligate : class {
    func btnItalianClicked(cell: UITableViewCell)
    func btnChineseClicked(cell: UITableViewCell)
    
}
class ResturantSearchCell: UITableViewCell {
    
    var resturantSearchDeligate : ResturantSearchDeligate?
    var dishdata = [String]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblResturantSubTitle: UILabel!
    @IBOutlet weak var lblResturantName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerImage(imageView: imgView)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        registerNib()
         self.collectionView.reloadData()
        
    }
//Func register nib
    func registerNib()
    {
        let nib = UINib(nibName: "DishTypeCollectionCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "DishTypeCollectionCell")
    }
    
    @IBAction func tapItalian(_ sender: Any) {
       if let mydeligate = self.resturantSearchDeligate
       {
        mydeligate.btnItalianClicked(cell: self)
        }
    }
    
    @IBAction func tapChinese(_ sender: Any) {
        if let mydeligate = self.resturantSearchDeligate
        {
            mydeligate.btnChineseClicked(cell: self)
        }
    }
}
extension ResturantSearchCell : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var text = dishdata[indexPath.row]
        return CGSize(width: 130, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dishdata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "DishTypeCollectionCell", for: indexPath) as? DishTypeCollectionCell
        {
        cell.lblDishType.text = dishdata[indexPath.row]
            return cell
    }
       return UICollectionViewCell()
    }
    
}
