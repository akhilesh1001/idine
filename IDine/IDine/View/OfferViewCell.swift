//
//  OfferViewCell.swift
//  Assignment6
//
//  Created by App on 05/12/18.
//  Copyright © 2018 Appventurez. All rights reserved.
//

import UIKit

class OfferViewCell: UICollectionViewCell {
    @IBOutlet weak var lblOfferDescription: UILabel!
    @IBOutlet weak var lblResturant: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblOffer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
