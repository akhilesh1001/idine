//
//  Resturant.swift
//  IDine
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import ObjectMapper

class Restaurant: NSObject, Mappable  {
    
    var Id: Int?
    var name: String?
    var address: String?
    var contactNumber : String?
    var status: String?
    var desc: String?
    var disheType = [String]()
    var openTime: String?
    var closeTime: String?
    var image: String?
    var fbUrl: String?
    var instUrl: String?
    var subtitle: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id <- map["id"]
        name <- map["r_name"]
        address <- map["r_address"]
        contactNumber <- map["r_contact"]
        status <- map["r_status"]
        desc <- map["r_desc"]
        disheType <- map["r_env"]
        openTime <- map["r_o_time"]
        closeTime <- map["r_c_time"]
        image <- map["r_img"]
        fbUrl <- map["fb_url"]
        instUrl <- map["inst_url"]
        subtitle <- map["sub_title"]
        
    }
    
}

extension Restaurant {
    
    
    class func parseData(data: [[String: AnyObject]]) -> [Restaurant]? {
        return Mapper<Restaurant>().mapArray(JSONArray:data)
    }
}
