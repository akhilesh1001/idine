//
//  Offer.swift
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import ObjectMapper


class Offer : NSObject,Mappable
{
    // MARK: - Properties
    
    var offerId: Int?
    var offerName: String?
    var offerStartDate: String?
    var offerEndDate : String?
    var OfferStatus: Bool?
    var refId: String?
    var offerDescription : String?
    var offerImage: String?
    var rname: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        offerId <- map["offer_id"]
        offerName <- map["offer_name"]
        offerStartDate <- map["offer_startdate"]
        offerEndDate <- map["offer_enddate"]
        OfferStatus <- map["offer_status"]
        refId <- map["ref_id"]
        offerDescription <- map["offer_descript"]
        offerImage <- map["offer_image"]
        rname <- map["r_name"]
    }
    
}

extension Offer
{
    class func parseData(data: [[String: AnyObject]]) -> [Offer]? {
        return Mapper<Offer>().mapArray(JSONArray:data)
    }
}

