//
//  Headers.swift
//  IDine
//
//  Created by Akhilesh Gupta on 07/12/18.
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation

let  kBaseUrl: String! = "http://13.233.182.208:8081/"
let kLoginUrl: String! = kBaseUrl + "login"
let kHomeBannerUrl: String! = kBaseUrl + "banner"
let kResendOtpUrl: String! = kBaseUrl + "resend-otp"
let kMembershipUrl: String! = kBaseUrl + "membership"
let kResturantUrl: String! = kBaseUrl + "restaurants"
let KrResturantDetailUrl: String! = kBaseUrl + "restaurant-details"
let kOfferUrl: String! = kBaseUrl + "offer"
let kTellUsUrl: String! = kBaseUrl + "tell-us"
let kSignupUrl: String! = kBaseUrl + "signup"

