//
//  RestaurantDetail.swift
//  Copyright © 2018 appventurez. All rights reserved.
//

import Foundation
import ObjectMapper

class RestaurantDetail: NSObject, Mappable  {
    
    //MARK: - Properties
    
    var Id: Int?
    var name: String?
    var address: String?
    var contactNumber : String?
    var status: String?
    var desc: String?
    var disheType = [String]()
    var openTime: String?
    var closeTime: String?
    var image: String?
    var fbUrl: String?
    var instUrl: String?
    var subtitle: String?
    var offer = [OfferData]()
    
    //MARK: - Mappble  Delegate Method
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Id <- map["id"]
        name <- map["r_name"]
        address <- map["r_address"]
        contactNumber <- map["r_contact"]
        status <- map["r_status"]
        desc <- map["r_desc"]
        disheType <- map["r_env"]
        openTime <- map["r_o_time"]
        closeTime <- map["r_c_time"]
        image <- map["r_img"]
        fbUrl <- map["fb_url"]
        instUrl <- map["inst_url"]
        subtitle <- map["sub_title"]
        offer <- map["offer"]
    }
    
    
    class OfferData: Mappable{
        
        required init?(map: Map) {
        }
        
       // MARK: - Properties
        var offerId : Int?
        var offername: String?
        var startDate: String?
        var endDate: String?
        var description: String?
        var restaurantName: String?
        var image: String?
        
        func mapping(map: Map) {
            
            offerId <- map["offer_id"]
            offername <- map["offer_name"]
            startDate <- map["offer_startdate"]
            endDate <- map["offer_enddate"]
            description <- map["offer_descript"]
            restaurantName <- map["r_name"]
            image <- map["offer_image"]
        }
        
        
    }
}

extension RestaurantDetail {
    
    
    class func parseData(data: [[String: AnyObject]]) -> [RestaurantDetail]? {
        return Mapper<RestaurantDetail>().mapArray(JSONArray:data)
    }
}

