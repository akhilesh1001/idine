//
//  APIDetail.swift
//  BConnected
//
//  Created by Ashish on 08/11/17.
//  Copyright © 2017 BConnected. All rights reserved.
//

import Foundation
import Alamofire
//import SwiftyUserDefaults
//import ObjectMapper

struct APIDetail {
    
    var path: String = ""
    var parameter: APIParams = APIParams()
    var method: Alamofire.HTTPMethod = .get
    var encoding: ParameterEncoding = URLEncoding.default
    var isBaseUrlNeedToAppend: Bool = true
    var showLoader: Bool = true
    var showAlert: Bool = true
    var showMessageOnSuccess = false
    var isHeaderTokenRequired: Bool = true
    var isPageNoInHeaderRequired : Bool = true
    var supportOffline = false
    var isUserIdInHeaderRequired = true
    var isUserTypeInHeaderRequired = true
    var fullResponse = false
    
    //method = .put
    //encoding = JSONEncoding.default
    
    //method = .delete
    //encoding = URLEncoding.default
    
    init(endpoint: APIEndpoint) {
        
        switch endpoint {
            
        case .login(let param):
            path = "login"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .resendOtp(let param):
            path = "resend-otp"
             parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
    
        case .signup(let param):
            path = "signup"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = false
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .offer():
            path = "offer"
            method = .get
            encoding = URLEncoding.default
            isHeaderTokenRequired = true
            isPageNoInHeaderRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .getResturant():
            path = "restaurants"
            method = .get
            encoding = URLEncoding.default
            isPageNoInHeaderRequired = true
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .membershipRequest():
            path = "membership"
            method = .get
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .getHomeBanner():
            path = "banner"
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            isUserTypeInHeaderRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .getResturantDetail(let param):
            path = "restaurant-details"
            parameter = param
            method = .post
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .editPhone(let param):
            path = "resend-otp"
            parameter = param
            method = .get
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        case .tellMeAboutYourself(let param):
            path = "tell-us"
            parameter = param
            method = .get
            encoding = JSONEncoding.default
            isHeaderTokenRequired = true
            showAlert = true
            isUserIdInHeaderRequired = false
            
        default:
            break
        }
    }
}
