//
//  NetworkEngine.swift
//  sympl2.02
//
//  Created by Appy on 23/10/18.
//  Copyright © 2018 Jacob Sock. All rights reserved.
//

import UIKit
import Alamofire
import SwiftHTTP

class NetworkEngine: NSObject {
    
    static let sharedInstance = NetworkEngine()
    
    
    func postRequestHandler(_ urlString: String!,isBaseAuthorized: Bool!, params:Dictionary<String,AnyObject>!,completionHandler:@escaping (AnyObject?, NSError?, Int?)->()) ->() {
        
        #if DEBUG
        print("HTTP METHOD: POST")
        print("REQUEST URL : \(urlString)")
        print("PARAMS : \(params)")
        
        #endif
        
        var headers = [
            "content-type": "application/json",
         ]

      if let token = UserDefaults.standard.object(forKey: "token") as? String {
        headers["token"] = token
        print(token)
       }
        
        Alamofire.request(
            URL(string: urlString)!,
            method: .post,
            parameters: params, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    completionHandler(nil, response.result.error as NSError? ,response.response?.statusCode)
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
                    print("Invalid tag information received from the service")
                    completionHandler(nil, response.result.error as NSError?,response.response?.statusCode)
                    return
                }
                
                #if DEBUG
                print("responseJSON")
                print(responseJSON)
                #endif
                completionHandler(responseJSON as AnyObject, nil,response.response?.statusCode)
                    }
       }
    
func postRequestHandlerSignup(_ urlString: String!,isBaseAuthorized: Bool!, params:Dictionary<String,AnyObject>!,completionHandler:@escaping (AnyObject?, NSError?, Int?)->()) ->() {
    
    #if DEBUG
    print("HTTP METHOD: POST")
    print("REQUEST URL : \(urlString)")
    print("PARAMS : \(params)")
    
    #endif
    
    var headers = [
        "content-type": "application/json",
        ]
    
    if let token = UserDefaults.standard.object(forKey: "token") as? String {
        headers["token"] = token
        print(token)
    }
    
    Alamofire.request(
        URL(string: urlString)!,
        method: .post,
        parameters: params, encoding: JSONEncoding.default, headers: headers)
        .responseJSON { (response) -> Void in
            guard response.result.isSuccess else {
                print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                completionHandler(nil, response.result.error as NSError? ,response.response?.statusCode)
                return
            }
            
            guard let responseJSON = response.result.value as? [String: Any] else {
                print("Invalid tag information received from the service")
                completionHandler(nil, response.result.error as NSError?,response.response?.statusCode)
                return
            }
            
            #if DEBUG
            print("responseJSON")
            print(responseJSON)
            #endif
            completionHandler(responseJSON as AnyObject, nil,response.response?.statusCode)
    }
}
    func postRequestHandlerGetHomeBanner(_ urlString: String!,isBaseAuthorized: Bool!,completionHandler:@escaping (AnyObject?, NSError?, Int?)->()) ->() {
        
        #if DEBUG
        print("HTTP METHOD: POST")
        print("REQUEST URL : \(urlString)")
       // print("PARAMS : \(params)")
        
        #endif
        
        var headers = [
            "content-type": "application/json",
            ]
        
        if let token = UserDefaults.standard.object(forKey: "token") as? String {
            headers["token"] = token
            print(token)
        }
        
        Alamofire.request(
            URL(string: urlString)!,
            method: .post,
            //parameters: params,
            encoding: JSONEncoding.default, headers: headers)
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    completionHandler(nil, response.result.error as NSError? ,response.response?.statusCode)
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
                    print("Invalid tag information received from the service")
                    completionHandler(nil, response.result.error as NSError?,response.response?.statusCode)
                    return
                }
                
                #if DEBUG
                print("responseJSON")
                print(responseJSON)
                #endif
                completionHandler(responseJSON as AnyObject, nil,response.response?.statusCode)
        }
    }
    
    func getRequestHandler(_ urlString: String!,isBaseAuthorized: Bool!,  completionHandler:@escaping (AnyObject?, NSError?, Int?)->()) ->()
    {
    
    #if DEBUG
    print("HTTP METHOD: GET")
    print("REQUEST URL : \(urlString)")
    #endif
    
    
    var headers = [
    "content-type": "application/json",
    ]
    
        if let token = UserDefaults.standard.object(forKey: "token") as? String{
            headers["token"] = token
            print(token)
            headers["user_type"] = "member"
            headers["page_no"] =  "1"
        }
    
    Alamofire.request(
    URL(string: urlString)!,
    method: .get,
    parameters: nil, encoding: JSONEncoding.default, headers: headers)
    .validate()
    .responseJSON { (response) -> Void in
    guard response.result.isSuccess else {
    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
    completionHandler(nil, response.result.error as NSError? ,response.response?.statusCode)
    return
    }
    
    guard let responseJSON = response.result.value as? [String: Any] else {
    print("Invalid tag information received from the service")
    completionHandler(nil, response.result.error as NSError?,response.response?.statusCode)
    return
    }
    
    #if DEBUG
    print("responseJSON")
    print(responseJSON)
    #endif
    completionHandler(responseJSON as AnyObject, nil,response.response?.statusCode)
    
    }
    }
}

